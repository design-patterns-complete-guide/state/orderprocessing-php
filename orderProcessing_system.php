<?php
// Define the State interface
interface OrderState
{
    public function processOrder(Order $order);
    public function shipOrder(Order $order);
    public function deliverOrder(Order $order);
}
// Concrete state classes
class PendingState implements OrderState
{
    public function processOrder(Order $order)
    {
        echo "Processing the order.\n";
        $order->setState(new ProcessingState());
    }
    public function shipOrder(Order $order)
    {
        echo "Cannot ship a pending order.\n";
    }
    public function deliverOrder(Order $order)
    {
        echo "Cannot deliver a pending order.\n";
    }
}
class ProcessingState implements OrderState
{
    public function processOrder(Order $order)
    {
        echo "Order is already being processed.\n";
    }
    public function shipOrder(Order $order)
    {
        echo "Shipping the order.\n";
        $order->setState(new ShippedState());
    }
    public function deliverOrder(Order $order)
    {
        echo "Cannot deliver an order that hasn't been shipped.\n";
    }
}
class ShippedState implements OrderState
{
    public function processOrder(Order $order)
    {
        echo "Cannot process a shipped order.\n";
    }
    public function shipOrder(Order $order)
    {
        echo "Order is already shipped.\n";
    }
    public function deliverOrder(Order $order)
    {
        echo "Delivering the order.\n";
        $order->setState(new DeliveredState());
    }
}
class DeliveredState implements OrderState
{
    public function processOrder(Order $order)
    {
        echo "Cannot process a delivered order.\n";
    }
    public function shipOrder(Order $order)
    {
        echo "Cannot ship a delivered order.\n";
    }
    public function deliverOrder(Order $order)
    {
        echo "Order is already delivered.\n";
    }
}
// Context class
class Order
{
    private $state;
    public function __construct()
    {
        $this->state = new PendingState(); // Initial state is "Pending"
    }
    public function setState(OrderState $state)
    {
        $this->state = $state;
    }
    public function process()
    {
        $this->state->processOrder($this);
    }
    public function ship()
    {
        $this->state->shipOrder($this);
    }
    public function deliver()
    {
        $this->state->deliverOrder($this);
    }
}
// Example usage
$order = new Order();
$order->process(); // Output: Processing the order.
$order->ship();    // Output: Shipping the order.
$order->deliver(); // Output: Delivering the order.
$order->ship();    // Output: Cannot ship a delivered order.
